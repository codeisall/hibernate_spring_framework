<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<title>Information</title>
</head>
<body>
	<h1 align="center">Your information</h1>
	<table align=center>
		<tr>
			<td>Name : </td>
			<td>${student.name}</td>
		</tr>
		<tr>
			<td>ID : </td>
			<td>${student.id}</td>
		</tr>
		<tr>
			<td>Age : </td>
			<td>${student.age}</td>
		</tr>
	</table>
</body>
</html>