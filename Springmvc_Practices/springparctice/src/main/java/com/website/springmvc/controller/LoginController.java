package com.website.springmvc.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {
	@RequestMapping(value = "/views/", method = RequestMethod.GET)
	public String onload() {
		return "login";
	}

	@RequestMapping(value = "/views/login", method = RequestMethod.POST)
	public String login(@RequestParam String userName, @RequestParam String Id, @RequestParam int age) {
		//Em muốn check xem có chứa bất kì khoảng trắng nào không
		String pattern = "[^s]";
		Pattern pt = Pattern.compile(pattern);
		Matcher mc = pt.matcher(userName);
		if (!mc.find())
			return "redirect:/views/info/" + userName + "/" + Id + "/" + age;
		return "login";
	}
}