package com.website.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.website.springmvc.config.SpringWebConfig;
import com.website.springmvc.entities.Student;

@Controller
@RequestMapping(value = "/views/info")
public class HomeController {
	
	@RequestMapping(value = "/{name}/{id}/{age}", method = RequestMethod.GET)
	public ModelAndView printWelcome(@PathVariable("name") String name, @PathVariable("id") String id, @PathVariable("age") int age) {
		ModelAndView model = new ModelAndView();
		ApplicationContext ac = new AnnotationConfigApplicationContext(SpringWebConfig.class);
		Student student = ac.getBean(Student.class);
		student = new Student(name, id, age);
		model.setViewName("info");
		model.addObject("student", student);
		return model;
	}
}