package com.website.springmvc.entities;

public class Student {
	private String name;
	private String id;
	private int age;
	
	public Student(String name, String id, int age) {
		super();
		this.name = name;
		this.id = id;
		this.age = age;
	}
	
	public Student() {
		super();
		this.name = "";
		this.id = "";
		this.age = 0;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	
}
