package com.website.springmvc.config;

import javax.transaction.Transactional.TxType;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.sun.xml.fastinfoset.sax.Properties;
import com.website.springmvc.entity.Address;
import com.website.springmvc.entity.Course;
import com.website.springmvc.entity.Student;
@EnableTransactionManagement
@Configuration
public class SpringDatabaseConfig extends WebMvcConfigurerAdapter{
	@Bean
	public LocalSessionFactoryBean sessionFactory(BasicDataSource dataSource){
		LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
		sessionFactoryBean.setDataSource(dataSource);
		sessionFactoryBean.setPackagesToScan(new String[] {"com.website.springmvc.entities"});
		sessionFactoryBean.setAnnotatedClasses(Student.class, Address.class, Course.class);
		return sessionFactoryBean;
	}
	
	private java.util.Properties hibernatePropperties(){
		java.util.Properties properties = new java.util.Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.show_sql", true);
		properties.put("hibernate.formart_sql", true);
		properties.put("hibernate.hbm2ddl.auto", "update");
		return properties;
	}
	
	@Bean
	public BasicDataSource getDataSource(){
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql//localhost:3306/website");
		dataSource.setUsername("root");
		dataSource.setPassword("");
		dataSource.setInitialSize(10);
		return dataSource;
	}
	
	@Bean
	public HibernateTransactionManager transactionManager(SessionFactory s){
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(s);
		return txManager;
	}
}
