package com.website.springmvc.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.website.springmvc.entity.Student;

import hibernate.dao.EntityDAO;

@Transactional
@Service
public class StudentService {
	@Autowired
	EntityDAO<Student> studentDao;

	public List<Student> getAll() {
		return studentDao.getAll();
	}

	public Student get(Long id) {
		return studentDao.get(id);
	}

	public Student insert(Student student) {
		try {
			return studentDao.insert(student);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return student;
	}

	public boolean update(Student student) {
			try {
				return studentDao.update(student);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
	}
	
	public boolean delete(Student student){
		try {
			return studentDao.delete(student);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean delect(Long id){
		try {
			return studentDao.delect(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
