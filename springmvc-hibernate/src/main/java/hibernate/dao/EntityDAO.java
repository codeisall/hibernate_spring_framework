package hibernate.dao;

import java.util.List;

import com.website.springmvc.entity.Student;

public abstract class EntityDAO<T> {
	
	public abstract List<T> getAll();	
	public abstract T get(long id);
	public abstract T insert(T t) throws Exception;
	public abstract boolean update(T t) throws Exception;
	public abstract boolean delete(T t) throws Exception;
	public abstract boolean delect(long id) throws Exception;
}
