package hibernate.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.website.springmvc.entity.Address;
import com.website.springmvc.entity.Clazz;

public class AddressDAO extends EntityDAO<Address> {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Address> getAll() {
		try {
			Session session = sessionFactory.getCurrentSession();
			List<Address> addresses = session.createQuery("from Adress").list();
			
			return addresses;
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}

	@Override
	public Address get(long id) {
		try {
			Session session = sessionFactory.getCurrentSession();
			/*Criteria criteria = session.createCriteria(Address.class);
			Address result = (Address)criteria.add(Restrictions.idEq(id));
			session.getTransaction().commit();*/
			Address result = (Address) session.createQuery("from Adress");
			
			return result;
		} catch (Exception e) {
			return null;
		}	
	}

	@Override
	public Address insert(Address t) throws Exception{
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(t);
			return t;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean update(Address t) throws Exception{
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean delete(Address t) throws Exception {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.delete(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean delect(long id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		Address address = session.load(Address.class, new Long(id));
		if (null != address) {
			session.delete(address);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}
