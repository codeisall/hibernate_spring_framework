package hibernate.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import com.website.springmvc.entity.Clazz;

public class ClazzDAO extends EntityDAO<Clazz>{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Clazz> getAll() {
		try {
			Session session = sessionFactory.getCurrentSession();
			List<Clazz> clazzes = session.createQuery("from Class").list();
			return clazzes;
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}

	@Override
	public Clazz get(long id) {
		try {
			Session session = sessionFactory.getCurrentSession();
			Clazz clazz = (Clazz) session.createQuery("from Class");

			return clazz;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Clazz insert(Clazz t) throws Exception {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(t);
			return t;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean update(Clazz t) throws Exception {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean delete(Clazz t) throws Exception {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.delete(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean delect(long id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		Clazz clazz = session.load(Clazz.class, new Long(id));
		if (null != clazz) {
			session.delete(clazz);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
