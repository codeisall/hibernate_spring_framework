package hibernate.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.website.springmvc.entity.Student;

public class StudentDAO extends EntityDAO<Student> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Student> getAll() {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			List<Student> students = session.createQuery("fromStudent").list();

			return students;
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}

	@Override
	public Student get(long id) {
		Session session = this.sessionFactory.getCurrentSession();

		return session.get(Student.class, new Long(id));
	}

	@Override
	public Student insert(Student t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(t);
		return t;
	}

	@Override
	public boolean update(Student t){
		Session session = this.sessionFactory.getCurrentSession();
		session.update(t);
		return Boolean.TRUE;
	}

	@Override
	public boolean delete(Student t){
		Session session = this.sessionFactory.getCurrentSession();
		if (null != t) {
			session.delete(t);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean delect(long id){
		Session session = this.sessionFactory.getCurrentSession();
		Student student = session.load(Student.class, new Long(id));
		if (null != student) {
			session.delete(student);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
