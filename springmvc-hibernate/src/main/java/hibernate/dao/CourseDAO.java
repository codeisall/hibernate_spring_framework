package hibernate.dao;

import java.util.Collections;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.website.springmvc.entity.Course;
import com.website.springmvc.entity.Student;

public class CourseDAO extends EntityDAO<Course> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Course> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Course> courses = session.createQuery("from Course").list();
		return courses;
	}

	@Override
	public Course get(long id) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Course course = (Course) session.createQuery("from Course");

			return course;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Course insert(Course t) throws Exception {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.save(t);
			return t;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean update(Course t) throws Exception {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean delete(Course t) throws Exception {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.delete(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean delect(long id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		Course course = session.load(Course.class, new Long(id));
		if (null != course) {
			session.delete(course);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
