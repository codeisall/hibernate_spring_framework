<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="jquery-3.3.1.min.js"></script>
<title>Insert title here</title>
</head>
<body>
	<div class="panel panel-default">
		<div id="abc" class="panel-heading h3 text-center">
			<spring:message code="student.headr" />
		</div>
		<div class="panel-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><spring:message code="student.table.id" /></th>
						<th><spring:message code="student.table.name" /></th>
						<th><spring:message code="student.table.age" /></th>
						<th><spring:message code="student.table.address" /></th>
						<th><spring:message code="student.table.action" /></th>
					</tr>
				</thead>
				<c:choose>
					<c:when test="${!empty students}">
						<c:forEach items="${student}" var="student">
							<tr>
								<td>${student.id}</td>
								<td>${student.firsname}${student.lastname}</td>
								<td>${student.age}</td>
								<td>${student.address.street}${student.address.district}
									${student.address.city}</td>
								<td>
									<button class="btn btn-info"
										onclick="getStudent(${student.id}, 'VIEW)';">
										<spring:message code="student.btn.view" />
									</button>
									<button class="btn btn-primary"
										onclick="getStudent(${student.id}, 'EDIT');">
										<spring:message code="student.btn.edit" />
									</button>
									<button class="btn btn-danger" onclick="delete(${student.id});">
										<spring:message code="student.btn.delete" />
									</button>
								</td>
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<tr>
							<th colspan="5" class="text-center"><spring:message
									code="student.table.empty" /></th>
						</tr>
					</c:otherwise>
				</c:choose>
				<tr>
					<th colspan="5">
						<button onclick="location.href='addStudent'"
							class="btn btn-primary">
							<spring:message code="student.btn.add" />
						</button>
					</th>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>